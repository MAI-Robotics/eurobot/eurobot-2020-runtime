FROM herobone/mai-ros:latest
# get MAI code
RUN ln -snf /bin/bash /bin/sh
RUN git clone https://gitlab.com/Herobone/eurobot-2020-ros.git
WORKDIR eurobot-2020-ros

RUN /bin/bash -c ". /opt/ros/melodic/setup.bash; catkin_make"
CMD source devel/setup.bash; roslaunch robot_bringup whole_bot.launch
